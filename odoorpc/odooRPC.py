import odoorpc
from datetime import date
import asyncio
import os
import discord
from dotenv import load_dotenv

#récupération de la date d'aujourd'hui
today= date.today()

#BEGINNING Discord Connection establishement
load_dotenv()
#gets the .env file and the token below
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
client = discord.Client(intents=discord.Intents.default())



async def send_avertissement(message):
	await client.wait_until_ready()
	#connection au canal avertissement
	channel = client.get_channel(1106859571554811916)
	await channel.send(message)



@client.event
async def on_ready():

	
	for guild in client.guilds:
		if guild.name == GUILD:
			break

	print(
		f'{client.user} is connected to the following guild:\n'
		f'{guild.name}(id: {guild.id})'
	)
	
	#connection au serveur odoo
	odoo = odoorpc.ODOO('localhost', port=8069)

	# Check available databases
	print(odoo.db.list())

	# Login
	odoo.login('VideoProd', 'juju', '1234')

	# Current user
	user = odoo.env.user
	print(user.name)            # name of the user connected
	print(user.company_id.name) # the name of its company

	#infinite loop, until quantum maths invade us
	while(1==1):
		if 'project.project' in odoo.env:

			print("project is here")
			project = odoo.env['project.project']
			ListProjects = project.search([])
			#list all projects and print their names and stages
			for project_id in project.browse(ListProjects):
				
				print(project_id.name)
				print(project_id.stage_id.name)
				
				
			task = odoo.env['project.task']
			
			ListTasks = task.search([])
			print(ListTasks)
			for task_id in task.browse(ListTasks):
				task_deadline= task_id.date_deadline
				print(task_deadline)
				if (task_deadline<today):
					task_user=task_id.user_ids
					print(task_user.name)
					print("delai depassé, sending avertissement to {}".format(task_user.name))
					message="{}, délai dépassé, vous êtes prié de présenter votre travail".format(task_user.name)
					await send_avertissement(message)
					
				else:
					print("délai ok")

		#waits for the next day				
		await asyncio.sleep(86400)
				


#finally this function run it all
client.run(TOKEN)
